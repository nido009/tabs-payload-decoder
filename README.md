# TABS Payloaddecoder f�r TTN

Hier liegt der Payloaddecoder f�r alle bis jetzt bekannten TABS Sensoren.
Als Basis dienen die Dokumentationen zu den Sensoren.

### Installing

Der Inhalt der Datei TABS_Payload_Decoder_TTN.js muss in das Feld Payload Formats eingef�gt werden.

## Authors

* **Frank Radzio*

## Version

* 0.1 Initialer Push


## License


## Bekannte Einschr�nkungen

* Zur Zeit werden noch nicht alle angelieferten Werte �bersetzt. Die fehlenden Decodierungen sind mit #Testdaten ersetzt so das nun schon mal alle Felder zur�ck gegeben werden.
   